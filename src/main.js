import Vue from 'vue'
import App from './App.vue'
import HighchartsVue from 'highcharts-vue';
import Highcharts from "highcharts";
/* Import the firebase SDK and extend with firestore */
import * as firebase from "firebase/app";
import "firebase/firestore";

// Firebase configuration
const firebaseConfig = {
  apiKey: process.env.VUE_APP_FB_API_KEY,
  authDomain: process.env.VUE_APP_FB_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_FB_DB_URL,
  projectId: process.env.VUE_APP_FB_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FB_STORE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FB_MESSAGE_SENDER_ID,
  appId: process.env.VUE_APP_FB_APP_ID,
  measurementId: process.env.VUE_APP_FB_MEASUREMENT_ID
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


/* Bind firebase to Vue instance */
Vue.prototype.$db = firebase.firestore()

Vue.config.productionTip = false
Vue.use(HighchartsVue, {
  highcharts: Highcharts
});

new Vue({
  render: h => h(App),
}).$mount('#app')
